drop database if exists kuirado;
create database kuirado
    default character set utf8mb4
    default collate utf8mb4_unicode_ci;
use kuirado;

create table user(
id int unsigned not null primary key auto_increment,
-- char vs varchar
email varchar(255) not null,
password char(72) not null
);

create table persistent_logins (
id int unsigned not null primary key auto_increment,
userid varchar(255) not null,
token varchar(64) not null
);

create table ingredient(
id int unsigned not null primary key auto_increment,
name varchar(255) not null,
category char(32) not null
);

create table recipe(
id int unsigned not null primary key auto_increment,
name varchar(255) not null,
description text not null,
preparationTime int unsigned not null,
difficulty_level char(32) not null
);

create table fk_recipe_ingredient(
id int unsigned not null primary key auto_increment,
recipe_id int unsigned not null,
ingredient_id int unsigned not null,
amount varchar(255) not null,
foreign key (recipe_id) references recipe(id),
foreign key (ingredient_id) references ingredient(id)
);


create table tag(
id int unsigned not null primary key auto_increment,
name varchar(255) not null
);

create table recipe_tag(
recipe_id int unsigned not null,
foreign key (recipe_id) references recipe(id),
tag_id int unsigned not null,
foreign key (tag_id) references tag(id)
);

insert into tag values(null, "obiad");
insert into tag values(null, "kolacja");
insert into tag values(null, "wegetariańskie");
