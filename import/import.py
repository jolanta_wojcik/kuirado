from aifc import Error

import requests
import mysql.connector
import re

S = requests.Session()

API_PATH = "https://en.wikibooks.org/w/api.php"

title = "Cookbook:Chicken_Tikka_Masala"

# //1 - ingredient
# //2 - procedure
ingredient_section = 1
procedure_section = 2


def runAPI(section, title):
    params = {'action': "parse",'page': title,'section': section,'format': "json"}
    r = S.get(url=API_PATH, params=params)
    data = r.json()
    text = data['parse']['text']['*']
    cleaned_text = cleancatch(cleanhtml(text))
    return ''.join(cleaned_text.strip().splitlines(True)[1:])

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def cleancatch(raw_html):
    cleanr = re.compile('<!--(\s|.)*-->')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


data1 = runAPI(ingredient_section, title)
data2 = runAPI(procedure_section, title)


from bs4 import BeautifulSoup
url="https://en.wikibooks.org/wiki/Cookbook:Chicken_Tikka_Masala"
r = requests.get(url)
html_content = r.text
soup = BeautifulSoup(html_content, "html.parser")
table = soup.find("table", attrs={"class":"infobox"})

for tr in table:
    tds = tr.find_all('td')
    ths = tr.find_all('th')
#tr1 title
    title = ths[0].text
#tr3 tag
    tag = tds[1].text
#tr5 czas
    time = (tds[3].text).rsplit(' ', 1)[0]
#tr6 title trudnosc
    for col in tds[4]:
            hrefs = col.find_all('a')
            for href in hrefs:
                difficulty = href.get('title')

print(title)
print(tag)
print(time)
print(difficulty)
print(data1)
print(data2)

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="zaqq",
  database="kuirado"
)

from mysql.connector import MySQLConnection, Error
def insert_recipe(name, description, preparationTime, difficulty_level):
    query = "INSERT INTO recipe(name, description, preparationTime, difficulty_level) " \
            "VALUES(%s,%s,%s, %s)"
    args = (title, data2, time, difficulty)

    try:
        cursor = mydb.cursor()
        cursor.execute(query, args)

        if cursor.lastrowid:
            print('last insert id', cursor.lastrowid)
        else:
            print('last insert id not found')

        mydb.commit()
    except Error as error:
        print(error)

    finally:
        cursor.close()
        mydb.close()

insert_recipe(title, data2, time, difficulty)
mycursor = mydb.cursor()
mycursor.execute("SELECT * FROM recipe")
myresult = mycursor.fetchall()
for x in myresult:
    print(x)

