package com.kuirado.controllers;

import com.kuirado.utils.ModelAndViewFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @Autowired
    ModelAndViewFactory modelAndViewFactory;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home() {
        return modelAndViewFactory.getModelAndView("home");
    }
}
