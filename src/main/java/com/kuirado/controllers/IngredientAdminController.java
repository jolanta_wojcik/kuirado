package com.kuirado.controllers;

import com.kuirado.models.Ingredient;
import com.kuirado.services.IngredientService;
import com.kuirado.utils.ModelAndViewFactory;
import com.kuirado.utils.PhotoManager;
import java.io.IOException;
import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IngredientAdminController {

    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private PhotoManager photoManager;

    @Autowired
    ModelAndViewFactory modelAndViewFactory;

    @RequestMapping(value = "/Admin/Ingredient", method = RequestMethod.GET)
    protected ModelAndView editIngredient(HttpServletRequest request) throws IOException {

        String id = request.getParameter("id");
        if (id != null) {
            int parsedId = Integer.parseInt(id);
            //    request.setAttribute("ingredientPhoto",  photoManager.getPhoto("ingredient/" + id));
            Ingredient ingredient = ingredientService.getIngredient(parsedId);
            request.setAttribute("ingredient", ingredient);
        }
        request.setAttribute("ingredientCategories", ingredientService.getIngredientCategory());
        return modelAndViewFactory.getModelAndView("ingredientAdd");
    }

    @RequestMapping(value = "/Admin/Ingredient", method = RequestMethod.POST)
    protected ModelAndView addIngredient(HttpServletRequest request,
            @RequestParam("ingredientPhoto") MultipartFile ingredientPhoto) throws IOException {
        String name = request.getParameter("name");
        String category = request.getParameter("category");
        String id = request.getParameter("id");

        LinkedList<String> errors = new LinkedList<>();

        if (name.equals("") || category.equals("")) {
            errors.add("Wszytskie pola muszą być wypełnione");
        }

        if (!ingredientService.isNameAllowed(name, parseId(id))) {
            errors.add("Składnik już istnieje");
        }

        if (errors.size() > 0) {
            ModelAndView model = modelAndViewFactory.getModelAndView("ingredientAdd");
            model.addObject("errors", errors);
            return model;
        } else {
            if (id.length() > 0) {
                int parsedId = Integer.parseInt(id);
                ingredientService.updateIngredient(parsedId, name, category);
            } else {
                int parsedId = ingredientService.addIngredient(name, category);
                id = Integer.toString(parsedId);
            }
        }
        if (!ingredientPhoto.isEmpty()) {
            photoManager.savePhoto(ingredientPhoto, "ingredient/" + id);
        }
        return new ModelAndView("redirect:../Ingredient");
    }

    @RequestMapping(value = "/Admin/Ingredient", method = RequestMethod.DELETE)
    public ModelAndView removeIngredient(HttpServletRequest request) throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        photoManager.removePhoto("ingredient/" + request.getParameter("id"));
        ingredientService.removeIngredient(id);
        return new ModelAndView("redirect:../Ingredient");
    }

    private int parseId(String possibleId) {
        try {
            return Integer.valueOf(possibleId);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
