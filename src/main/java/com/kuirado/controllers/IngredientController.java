package com.kuirado.controllers;

import com.kuirado.services.IngredientService;
import com.kuirado.utils.ModelAndViewFactory;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IngredientController {

    @Autowired
    private IngredientService ingredientService;

    @Autowired
    ModelAndViewFactory modelAndViewFactory;

    @RequestMapping(value = "/Ingredient", method = RequestMethod.GET)
    public ModelAndView showIngredients(HttpServletRequest request) {
        request.setAttribute("ingredients", ingredientService.getIngredients());
        return modelAndViewFactory.getModelAndView("ingredient");
    }
}
