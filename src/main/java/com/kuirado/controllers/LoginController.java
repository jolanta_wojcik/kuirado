package com.kuirado.controllers;

import com.kuirado.models.User;
import com.kuirado.services.UserLogin;
import com.kuirado.services.UserService;
import com.kuirado.utils.ModelAndViewFactory;
import java.util.LinkedList;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserLogin userLogin;

    @Autowired
    ModelAndViewFactory modelAndViewFactory;

    @RequestMapping(value = "/Login", method = RequestMethod.GET)
    public ModelAndView form(HttpServletRequest request) {
        // sprawdzam czy jest ciasteczko z tokenem
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("remember-me")) {
                    // jeżeli tak to wyciągam ID z bazy
                    // loguje na konto
                    if (!cookie.getValue().isEmpty()) {
                        String userId = userService.getUserIdByToken(cookie.getValue());
                        if (userId != null) {
                            User user = userService.presistanceLogin(userId);
                            setLoginInSession(request, user);
                            return returnToLastVisitedSite(request);
                        }
                    }
                }
            }
        }
        return modelAndViewFactory.getModelAndView("login");
    }

    @RequestMapping(value = "/Login", method = RequestMethod.POST)
    protected ModelAndView login(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String rememberMe = request.getParameter("remember-me");

        User user = userLogin.login(email, password);

        LinkedList<String> errors = new LinkedList<>();

        if (!EmailValidator.getInstance().isValid(email)) {
            errors.add("Adres email jest nie poprawny");
        }

        if (!userService.isPasswordCorrect(email, password)) {
            errors.add("Niepoprawne hasło");
        }

        if (errors.size() > 0) {
            ModelAndView model = modelAndViewFactory.getModelAndView("login");
            model.addObject("errors", errors);
            return model;
        } else {
            setLoginInSession(request, user);

            if (rememberMe != null) {
                String token = userService.rememberUser(user.getId().toString());
                Cookie presistanceCookie = new Cookie("remember-me", token);
                presistanceCookie.setMaxAge(60 * 60 * 24 * 30);
//                if(presistanceCookie.getMaxAge()==(60 * 60 * 24 * 30)){
//                    userService.presistanceLoginRemoveUser(token);
//                }
                response.addCookie(presistanceCookie);
            }

            return this.returnToLastVisitedSite(request);
        }
    }

    public void setLoginInSession(HttpServletRequest request, User user) {
        request.getSession().setAttribute("user", user);
    }

    public ModelAndView returnToLastVisitedSite(HttpServletRequest request) {
        String referer = (String) request.getSession().getAttribute("referer");
        if (referer == null) {
            return new ModelAndView("redirect:./");
        } else {
            return new ModelAndView("redirect:" + referer);
        }
    }
}
