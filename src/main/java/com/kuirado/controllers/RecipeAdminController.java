package com.kuirado.controllers;

import com.kuirado.models.Recipe;
import com.kuirado.services.IngredientService;
import javax.servlet.http.HttpServletRequest;
import com.kuirado.services.RecipeService;
import com.kuirado.utils.ModelAndViewFactory;
import com.kuirado.utils.PhotoManager;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RecipeAdminController {

    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private PhotoManager photoManager;

    @Autowired
    ModelAndViewFactory modelAndViewFactory;

    @RequestMapping(value = "/Admin/Recipe", method = RequestMethod.POST)
    protected ModelAndView addRecipe(HttpServletRequest request,
            @RequestParam("recipePhoto") MultipartFile recipePhoto) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String preparationTime = request.getParameter("preparationTime");
        String difficultyLevel = request.getParameter("difficultyLevel");
        String ingredient = request.getParameter("ingredients");
        String tags[] = request.getParameterValues("tags");
        if (tags == null) {
            tags = new String[]{};
        }

        //zapiac przepis
        String id = request.getParameter("id");
        if (id.length() > 0) {
            int parsedId = Integer.parseInt(id);
            recipeService.updateRecipe(parsedId, name, description, Integer.parseInt(preparationTime), difficultyLevel, ingredient, tags);
        } else {
            int parsedId = recipeService.addRecipe(name, description, Integer.parseInt(preparationTime), difficultyLevel, ingredient, tags);
            id = Integer.toString(parsedId);
        }
        if (!recipePhoto.isEmpty()) {
            photoManager.savePhoto(recipePhoto, "recipe/" + id);
        }
        return new ModelAndView("redirect:../Recipe");
    }

    @RequestMapping(value = "/Admin/Recipe", method = RequestMethod.GET)
    protected ModelAndView editRecipe(HttpServletRequest request) throws IOException {
        request.setAttribute("ingredients", ingredientService.getIngredients());
        String id = request.getParameter("id");
        if (id != null) {
            int parsedId = Integer.parseInt(id);
            Recipe recipe = recipeService.getRecipe(parsedId);
            request.setAttribute("recipe", recipe);
        }
        request.setAttribute("allTags", recipeService.getTags());
        request.setAttribute("difficultyLevels", recipeService.getRecipeDifficultyLevel());
        return modelAndViewFactory.getModelAndView("recipeAdd");
    }

    @RequestMapping(value = "/Admin/Recipe", method = RequestMethod.DELETE)
    protected ModelAndView removeRecipe(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        recipeService.removeRecipe(id);
        return new ModelAndView("redirect:../Recipe");
    }
}
