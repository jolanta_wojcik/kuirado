package com.kuirado.controllers;

import javax.servlet.http.HttpServletRequest;
import com.kuirado.services.RecipeService;
import com.kuirado.utils.ModelAndViewFactory;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RecipeController {

    @Autowired
    RecipeService recipeService;

    @Autowired
    ModelAndViewFactory modelAndViewFactory;

    @RequestMapping(value = "/Recipe", method = RequestMethod.GET)
    protected ModelAndView displayRecipes(HttpServletRequest request) {
        boolean isTheTag = false;
        request.setAttribute("flag", isTheTag);
        request.setAttribute("recipes", recipeService.getRecipes());
        return modelAndViewFactory.getModelAndView("recipe");
    }

    @RequestMapping(value = "/Tag/Meal", method = RequestMethod.GET)
    protected ModelAndView recipesByTag(HttpServletRequest request) throws IOException {
        String name = request.getParameter("name");
        boolean isTheTag = true;
        request.setAttribute("flag", isTheTag);
        request.setAttribute("recipes", recipeService.getRecipesByTag(name));
        return modelAndViewFactory.getModelAndView("recipe");
    }

    @RequestMapping(value = "/Tag/MostPopular", method = RequestMethod.GET)
    protected ModelAndView recipesByMostPopularTags(HttpServletRequest request) throws IOException {
        String name = request.getParameter("name");
        boolean isTheTag = true;
        request.setAttribute("flag", isTheTag);
        request.setAttribute("recipes", recipeService.getRecipesByTag(name));
        return modelAndViewFactory.getModelAndView("recipe");
    }
}
