package com.kuirado.controllers;

import com.kuirado.models.User;
import com.kuirado.services.UserRegister;
import com.kuirado.services.UserService;
import com.kuirado.utils.ModelAndViewFactory;

import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegisterController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRegister userRegister;

    @Autowired
    ModelAndViewFactory modelAndViewFactory;

    @RequestMapping(value = "/Register", method = RequestMethod.GET)
    public ModelAndView form() {
        return modelAndViewFactory.getModelAndView("register");
    }

    @RequestMapping(value = "/Register", method = RequestMethod.POST)
    protected ModelAndView doPost(HttpServletRequest request) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String retypePassword = request.getParameter("retype-password");

        LinkedList<String> errors = new LinkedList<>();

        if (password.length() < 6) {
            errors.add("Hasło jest za krótkie");
        }

        if (!password.equals(retypePassword)) {
            errors.add("Hasła są różne");
        }

        if (userService.isEmailTaken(email)) {
            errors.add("Użytkownik już istanieje");
        }

        if (!EmailValidator.getInstance().isValid(email)) {
            errors.add("Adres email jest nie poprawny");
        }

        if (errors.size() > 0) {
            ModelAndView model = modelAndViewFactory.getModelAndView("register");
            model.addObject("errors", errors);
            return model;
        } else {
            User user = userRegister.register(email, password);
            request.getSession().setAttribute("user", user);
            return new ModelAndView("redirect:./");
        }
    }

}
