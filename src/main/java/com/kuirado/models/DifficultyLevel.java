package com.kuirado.models;

public enum DifficultyLevel {
    EASY("łatwy"),
    MEDIUM("średni"),
    HARD("trudny");

    private final String text;

    DifficultyLevel(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public String getName() {
        return this.name();
    }

}
