package com.kuirado.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "fk_recipe_ingredient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FkRecipeIngredient.findAll", query = "SELECT f FROM FkRecipeIngredient f"),
    @NamedQuery(name = "FkRecipeIngredient.findById", query = "SELECT f FROM FkRecipeIngredient f WHERE f.id = :id"),
    @NamedQuery(name = "FkRecipeIngredient.findByAmount", query = "SELECT f FROM FkRecipeIngredient f WHERE f.amount = :amount")})
public class FkRecipeIngredient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "amount")
    private String amount;
    @JoinColumn(name = "recipe_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Recipe recipeId;
    @JoinColumn(name = "ingredient_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Ingredient ingredientId;

    public FkRecipeIngredient() {
    }

    public FkRecipeIngredient(Integer id) {
        this.id = id;
    }

    public FkRecipeIngredient(Integer id, String amount) {
        this.id = id;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Recipe getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Recipe recipeId) {
        this.recipeId = recipeId;
    }

    public Ingredient getIngredient() {
        return ingredientId;
    }

    public void setIngredient(Ingredient ingredientId) {
        this.ingredientId = ingredientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FkRecipeIngredient)) {
            return false;
        }
        FkRecipeIngredient other = (FkRecipeIngredient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.kuirado.models.FkRecipeIngredient[ id=" + id + " ]";
    }

}
