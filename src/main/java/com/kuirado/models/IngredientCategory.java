package com.kuirado.models;

public enum IngredientCategory {
    FRUITS("owoce"),
    VEGETABLES("warzywa"),
    DAIRY_PRODUCTS("nabiał");

    private final String text;

    IngredientCategory(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public String getName() {
        return this.name();
    }
}
