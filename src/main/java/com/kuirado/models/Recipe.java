package com.kuirado.models;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "recipe")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recipe.findAll", query = "SELECT r FROM Recipe r"),
    @NamedQuery(name = "Recipe.findById", query = "SELECT r FROM Recipe r WHERE r.id = :id"),
    @NamedQuery(name = "Recipe.findByName", query = "SELECT r FROM Recipe r WHERE r.name = :name"),
    @NamedQuery(name = "Recipe.findByPreparationTime", query = "SELECT r FROM Recipe r WHERE r.preparationTime = :preparationTime"),
    @NamedQuery(name = "Recipe.findByDifficultyLevel", query = "SELECT r FROM Recipe r WHERE r.difficultyLevel = :difficulty_level")
})
public class Recipe implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipeId")
    private Collection<FkRecipeIngredient> fkRecipeIngredientCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preparationTime")
    private int preparationTime;
    @JoinTable(name = "recipe_tag", joinColumns = {
        @JoinColumn(name = "recipe_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "tag_id", referencedColumnName = "id")})
    @ManyToMany
    private Set<Tag> tagSet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipeId")
    private Collection<FkRecipeIngredient> ingredientSet;
    @Column(name = "difficulty_level")
    private String difficultyLevel;

    public Recipe() {
    }

    public Recipe(Integer id) {
        this.id = id;
    }

    public Recipe(Integer id, String name, String description, int preparationTime, String difficultyLevel) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.preparationTime = preparationTime;
        this.difficultyLevel = difficultyLevel;
    }

    public String getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(String difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public String getDifficultyLevelValue() {
        return DifficultyLevel.valueOf(this.difficultyLevel).getText();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(int preparationTime) {
        this.preparationTime = preparationTime;
    }

    @XmlTransient
    public Set<Tag> getTagSet() {
        return tagSet;
    }

    public void setTagSet(Set<Tag> tagSet) {
        this.tagSet = tagSet;
    }

    @XmlTransient
    public Collection<FkRecipeIngredient> getIngredientSet() {
        return ingredientSet;
    }

    public void setIngredientSet(Collection<FkRecipeIngredient> ingredientSet) {
        for (FkRecipeIngredient i : ingredientSet) {
            i.setRecipeId(this);
        }
        this.ingredientSet = ingredientSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recipe)) {
            return false;
        }
        Recipe other = (Recipe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.kuirado.model.Recipe[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<FkRecipeIngredient> getFkRecipeIngredientCollection() {
        return fkRecipeIngredientCollection;
    }

    public void setFkRecipeIngredientCollection(Collection<FkRecipeIngredient> fkRecipeIngredientCollection) {
        this.fkRecipeIngredientCollection = fkRecipeIngredientCollection;
    }

}
