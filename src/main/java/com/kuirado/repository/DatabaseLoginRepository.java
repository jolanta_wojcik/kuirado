package com.kuirado.repository;

import com.kuirado.models.User;
import com.kuirado.utils.DatabaseManager;
import java.util.List;
import javax.persistence.EntityManager;

public class DatabaseLoginRepository implements LoginRepository {

    private DatabaseManager databaseManager;

    public void setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    @Override
    public User findByEmail(String email) {
        EntityManager em = this.databaseManager.createEntityManager();
        List<User> users = em.createNamedQuery("User.findByEmail", User.class)
                .setParameter("email", email)
                .getResultList();
        em.close();
        if (users.size() < 1) {
            return null;
        }
        return users.get(0);
    }
}
