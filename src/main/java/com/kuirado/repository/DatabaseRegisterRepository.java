package com.kuirado.repository;

import com.kuirado.models.User;
import com.kuirado.utils.DatabaseManager;
import javax.persistence.EntityManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class DatabaseRegisterRepository implements RegisterRepository {

    private DatabaseManager databaseManager;
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public void setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    @Override
    public User register(String email, String password) {
        EntityManager em = this.databaseManager.createEntityManager();
        em.getTransaction().begin();
        User user = new User();
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(password));
        em.persist(user);
        em.getTransaction().commit();
        em.close();
        return user;
    }

}
