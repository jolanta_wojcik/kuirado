package com.kuirado.repository;

import com.kuirado.models.User;

public interface LoginRepository {

    public User findByEmail(String email);
}
