package com.kuirado.repository;

import com.kuirado.models.User;

public interface RegisterRepository {

    public User register(String email, String password);
}
