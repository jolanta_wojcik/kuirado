package com.kuirado.services;

import com.kuirado.models.Ingredient;
import com.kuirado.models.IngredientCategory;
import com.kuirado.utils.DatabaseManager;
import java.util.List;
import javax.persistence.EntityManager;

public class IngredientService {

    private DatabaseManager databaseManager;

    public IngredientService() {
    }

    public void setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public boolean isNameAllowed(String name, int id) {
        EntityManager em = this.databaseManager.createEntityManager();
        try {
            Ingredient it = em.createNamedQuery("Ingredient.findByName", Ingredient.class).setParameter("name", name).getSingleResult();
            return it.getId() == id;
        } catch (javax.persistence.NoResultException e) {
            return true;
        }
    }

    public Ingredient getIngredient(int id) {
        EntityManager em = this.databaseManager.createEntityManager();
        Ingredient ingredient = em.find(Ingredient.class, id);
        em.close();
        return ingredient;
    }

    public List<Ingredient> getIngredients() {
        EntityManager em = this.databaseManager.createEntityManager();
        List<Ingredient> ingredients = em.createNamedQuery("Ingredient.findAll", Ingredient.class).getResultList();
        em.close();
        return ingredients;
    }

    public IngredientCategory[] getIngredientCategory() {
        return IngredientCategory.values();
    }

    public int addIngredient(String name, String category) {
        EntityManager em = this.databaseManager.createEntityManager();
        em.getTransaction().begin();
        Ingredient ingredient = new Ingredient();
        ingredient.setName(name);
        ingredient.setCategory(IngredientCategory.valueOf(category).name());
        em.persist(ingredient);
        em.getTransaction().commit();
        em.close();
        return ingredient.getId();
    }

    public void updateIngredient(int id, String name, String category) {
        EntityManager em = this.databaseManager.createEntityManager();
        em.getTransaction().begin();
        Ingredient ingredient = em.find(Ingredient.class, id);
        ingredient.setName(name);
        ingredient.setCategory(IngredientCategory.valueOf(category).name());
        em.persist(ingredient);
        em.getTransaction().commit();
        em.close();
    }

    public void removeIngredient(int id) {
        EntityManager em = this.databaseManager.createEntityManager();
        em.getTransaction().begin();
        Ingredient ingredient = em.find(Ingredient.class, id);
        em.remove(ingredient);
        em.getTransaction().commit();
        em.close();
    }

}
