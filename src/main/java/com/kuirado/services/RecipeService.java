package com.kuirado.services;

import com.kuirado.models.DifficultyLevel;
import com.kuirado.models.FkRecipeIngredient;
import com.kuirado.utils.DatabaseManager;
import com.kuirado.models.Ingredient;

import java.util.List;
import javax.persistence.EntityManager;
import com.kuirado.models.Recipe;
import com.kuirado.models.Tag;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class RecipeService {

    private DatabaseManager databaseManager;

    public RecipeService() {
    }

    public void setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public Recipe getRecipe(int id) {
        EntityManager em = this.databaseManager.createEntityManager();
        Recipe recipe = em.createQuery("SELECT DISTINCT r FROM Recipe r LEFT JOIN FETCH r.ingredientSet LEFT JOIN FETCH r.tagSet WHERE r.id = :id", Recipe.class)
                .setParameter("id", id)
                .getSingleResult();
        em.close();
        return recipe;
    }

    public List<Recipe> getRecipes() {
        EntityManager em = this.databaseManager.createEntityManager();
        List<Recipe> recipes = em.createQuery("SELECT DISTINCT r FROM Recipe r LEFT JOIN FETCH r.ingredientSet LEFT JOIN FETCH r.tagSet").getResultList();
        em.close();
        return recipes;
    }

    public List<Recipe> getRecipesByTag(String name) {
        EntityManager em = this.databaseManager.createEntityManager();
        List<Recipe> recipes = em.createQuery("SELECT DISTINCT r FROM Recipe r LEFT JOIN FETCH r.ingredientSet LEFT JOIN FETCH r.tagSet ts"
                + " WHERE ts.name = :name", Recipe.class)
                .setParameter("name", name)
                .getResultList();
        em.close();
        return recipes;
    }

    public List<Tag> getTags() {
        EntityManager em = this.databaseManager.createEntityManager();
        List<Tag> tags = em.createNamedQuery("Tag.findAll", Tag.class).getResultList();
        em.close();
        return tags;
    }

    private List<FkRecipeIngredient> parseIngredients(String ingredients) {
        EntityManager em = this.databaseManager.createEntityManager();
        String[] parts = ingredients.split(",");
        List<FkRecipeIngredient> list = new LinkedList<>();
        FkRecipeIngredient ingredient = new FkRecipeIngredient();
        for (int i = 0; i < parts.length; i += 2) {
            if (parts[i].length() > 0) {
                ingredient.setIngredient(em.find(Ingredient.class, Integer.parseInt(parts[i])));
                ingredient.setAmount(parts[i + 1]);
                list.add(ingredient);
            }
        }
        return list;
    }

    private void createTags(EntityManager em, String tagNames[]) {
        for (String tagName : tagNames) {
            List<Tag> tags = em.createNamedQuery("Tag.findByName", Tag.class).setParameter("name", tagName).getResultList();
            if (tags.isEmpty()) {
                Tag tag = new Tag();
                tag.setName(tagName);
                em.persist(tag);
                em.flush();
            }
        }
    }

    private Set<Tag> parseTags(EntityManager em, String tagNames[]) {
        createTags(em, tagNames);
        Set<Tag> list = new HashSet<>();
        for (String tagName : tagNames) {
            list.add(em.createNamedQuery("Tag.findByName", Tag.class).setParameter("name", tagName).getSingleResult());
        }
        return list;
    }

    public DifficultyLevel[] getRecipeDifficultyLevel() {
        return DifficultyLevel.values();
    }

    public int addRecipe(String name, String description, int preparationTime, String difficultyLevel, String ingredient, String[] tags) {
        EntityManager em = this.databaseManager.createEntityManager();
        em.getTransaction().begin();
        Recipe recipe = new Recipe();
        recipe.setName(name);
        recipe.setDescription(description);
        recipe.setPreparationTime(preparationTime);
        recipe.setDifficultyLevel(DifficultyLevel.valueOf(difficultyLevel).name());
        recipe.setIngredientSet(parseIngredients(ingredient));
        recipe.setTagSet(parseTags(em, tags));
        em.persist(recipe);
        em.getTransaction().commit();
        em.close();
        return recipe.getId();
    }

    public void updateRecipe(int id, String name, String description, int preparationTime, String difficultyLevel, String ingredient, String[] tags) {
        EntityManager em = this.databaseManager.createEntityManager();
        em.getTransaction().begin();
        Recipe recipe = em.find(Recipe.class, id);
        recipe.setName(name);
        recipe.setDescription(description);
        recipe.setPreparationTime(preparationTime);
        recipe.setDifficultyLevel(DifficultyLevel.valueOf(difficultyLevel).name());
        recipe.setIngredientSet(parseIngredients(ingredient));
        recipe.setTagSet(parseTags(em, tags));
        em.persist(recipe);
        em.getTransaction().commit();
        em.close();
    }

//    @Cascade(CascadeType.DELETE)
    public void removeRecipe(int id) {
        EntityManager em = this.databaseManager.createEntityManager();
        em.getTransaction().begin();
        Recipe recipe = em.find(Recipe.class, id);
//        Query q = em.createQuery("DELETE FROM recipe_tag WHERE recipe_id = :id")
//                .setParameter("id", id);
//        q.executeUpdate();
//        for (User user : group.users) {
//            user.groups.remove(group);
//        }
        em.remove(recipe);
//        for (Tag tag : recipe.getTagSet()) {
//            tag.getRecipeSet().remove(recipe);
//        }      
        //merge(em);
        em.flush();
        em.getTransaction().commit();
        em.close();
    }

}
