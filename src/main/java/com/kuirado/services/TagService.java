package com.kuirado.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.kuirado.models.Tag;
import com.kuirado.utils.DatabaseManager;

public class TagService {

    private DatabaseManager databaseManager;

    public void setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public List<Tag> getMostPopularTags() {
        EntityManager em = this.databaseManager.createEntityManager();
        List<Tag> tags = em.createQuery("SELECT t FROM Tag t ORDER BY SIZE(t.recipeSet) DESC", Tag.class).setMaxResults(3).getResultList();
        em.close();
        return tags;
    }
}
