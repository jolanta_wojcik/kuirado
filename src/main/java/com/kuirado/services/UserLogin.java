package com.kuirado.services;

import com.kuirado.models.User;
import com.kuirado.utils.PasswordEncoder;
import com.kuirado.repository.LoginRepository;

public class UserLogin {

    LoginRepository repository;
    PasswordEncoder encoder;

    public void setLoginRepository(LoginRepository repository) {
        this.repository = repository;
    }

    public void setPasswordEncoder(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    public User login(String email, String password) {
        User user = this.repository.findByEmail(email);
        if (user == null) {
            return null;
        }
        if (this.encoder.matches(password, user.getPassword())) {
            return user;
        }
        return null;
    }
}
