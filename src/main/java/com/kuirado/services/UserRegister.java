package com.kuirado.services;

import com.kuirado.models.User;
import com.kuirado.repository.RegisterRepository;

public class UserRegister {

    RegisterRepository repository;

    public void setRegisterRepository(RegisterRepository repository) {
        this.repository = repository;
    }

    public User register(String email, String password) {
        User user = this.repository.register(email, password);
        if (user == null) {
            return null;
        }
        return user;
    }
}
