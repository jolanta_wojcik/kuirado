package com.kuirado.services;

import com.kuirado.models.PersistentLogins;
import com.kuirado.models.User;
import com.kuirado.utils.DatabaseManager;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class UserService {

    private final SecureRandom random = new SecureRandom();
    private DatabaseManager databaseManager;
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public void setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public User presistanceLogin(String userId) {
        EntityManager em = this.databaseManager.createEntityManager();
        User user = em.find(User.class, Integer.valueOf(userId));
        return user;
    }

    public void presistanceLoginRemoveUser(String token) {
        EntityManager em = this.databaseManager.createEntityManager();
        em.getTransaction().begin();
        PersistentLogins user = em.createNamedQuery("PersistentLogins.findByToken", PersistentLogins.class)
                .setParameter("token", token).getSingleResult();
        em.remove(user);
        em.getTransaction().commit();
        em.close();
    }

    public boolean isEmailTaken(String email) {
        User user = this.findByEmail(email);
        // return user != null
        if (user == null) {
            return false;
        }
        return true;
    }

    public boolean isPasswordCorrect(String email, String password) {
        User user = this.findByEmail(email);
        if (user != null) {
            if (passwordEncoder.matches(password, user.getPassword())) {
                return true;
            }
        }
        return false;
    }

    private User findByEmail(String email) {
        EntityManager em = this.databaseManager.createEntityManager();
        List<User> users = em.createNamedQuery("User.findByEmail", User.class)
                .setParameter("email", email)
                .getResultList();
        em.close();
        if (users.size() < 1) {
            return null;
        }
        return users.get(0);
    }

    public String rememberUser(String userid) {
        String randomId = new BigInteger(130, random).toString(32);
        EntityManager em = this.databaseManager.createEntityManager();
        em.getTransaction().begin();
        PersistentLogins persistentLogins = new PersistentLogins();
        persistentLogins.setUserid(userid);
        persistentLogins.setToken(randomId);
        em.persist(persistentLogins);
        em.getTransaction().commit();
        em.close();
        return randomId;
    }

    public String getUserIdByToken(String token) {
        EntityManager em = this.databaseManager.createEntityManager();
        PersistentLogins persistentLogins;
        String userId = null;
        try {
            persistentLogins = em.createNamedQuery("PersistentLogins.findByToken", PersistentLogins.class)
                    .setParameter("token", token).getSingleResult();
            userId = persistentLogins.getUserid();
        } catch (NoResultException nre) {
            return null;
        }
        em.close();
        return userId;
    }

}
