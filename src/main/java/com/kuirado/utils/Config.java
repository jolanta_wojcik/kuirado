package com.kuirado.utils;

import io.github.cdimascio.dotenv.Dotenv;
import javax.servlet.ServletContext;

public class Config extends Dotenv {

    private Dotenv config;
    private ServletContext servletContext;

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public String get(String string) {
        if (this.config == null) {
            String realPath = this.servletContext.getRealPath("/");
            String envPath = realPath + "../../";
            this.config = Dotenv.configure().directory(envPath).ignoreIfMissing().load();
        }
        return this.config.get(string);
    }

}
