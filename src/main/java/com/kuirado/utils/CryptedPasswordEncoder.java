package com.kuirado.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class CryptedPasswordEncoder implements PasswordEncoder {

    @Override
    public Boolean matches(String password, String encryptedPassword) {
        return (new BCryptPasswordEncoder()).matches(password, encryptedPassword);
    }

    public String encodedPassword(String encryptedPassword) {
        return (new BCryptPasswordEncoder()).encode(encryptedPassword);
    }

}
