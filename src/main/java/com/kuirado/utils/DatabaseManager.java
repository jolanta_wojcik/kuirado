package com.kuirado.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DatabaseManager {

    private EntityManagerFactory emf;

    public DatabaseManager() {
        this.emf = Persistence.createEntityManagerFactory("KuiradoPU");
    }

    public EntityManager createEntityManager() {
        return this.emf.createEntityManager();
    }
}
