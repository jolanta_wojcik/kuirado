package com.kuirado.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.kuirado.services.TagService;

public class ModelAndViewFactory {

    @Autowired
    TagService tagService;

    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    public ModelAndView getModelAndView(String view) {
        ModelAndView model = new ModelAndView();
        model.addObject("popularTags", tagService.getMostPopularTags());
        model.setViewName("/views/mainView.jsp");
        model.addObject("view", view + ".jsp");
        return model;
    }
}
