package com.kuirado.utils;

public interface PasswordEncoder {

    public Boolean matches(String password, String encryptedPassword);

    public String encodedPassword(String encryptedPassword);
}
