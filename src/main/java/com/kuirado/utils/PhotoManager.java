package com.kuirado.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.web.multipart.MultipartFile;

public class PhotoManager {

    private Config config;

    public void setConfig(Config config) {
        this.config = config;
    }

    public void savePhoto(MultipartFile file, String name) throws IOException {
        byte[] bytes = file.getBytes();
        Path path = Paths.get(this.config.get("UPLOADED_FOLDER") + name);
        Files.write(path, bytes);
    }

    public void savePhotoFromApi(File recipePhoto, String name) throws IOException {
        // Path path = Paths.get(this.config.get("UPLOADED_FOLDER") + name);
        String path = this.config.get("UPLOADED_FOLDER") + name;
        File file = new File(recipePhoto, path);
    }

    public File getPhoto(String name) throws IOException {
        return new File(this.config.get("UPLOADED_FOLDER") + name);
    }

    public void editPhoto(MultipartFile file, String name) throws IOException {
        removePhoto(name);
        savePhoto(file, name);
    }

    public void removePhoto(String name) throws IOException {
        File file = new File(this.config.get("UPLOADED_FOLDER") + name);
        file.delete();
    }
}
