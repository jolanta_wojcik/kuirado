<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table>
    <tr>
        <td>Id</td>
        <td>Name</td>
        <td>Category</td>
        <td>Photo</td>
        <td>Actions</td>
    </tr>
    <c:forEach items="${ingredients}" var="ingredient">
        <tr>
            <td>${ingredient.id}</td>
            <td class="ingredient-name" name="name">${ingredient.name}</td>
            <td>${ingredient.categoryValue}</td>
            <td><img src="photos/ingredient/${ingredient.id}" width="100"></td>
            <td>
                <a href="<c:url value="/Admin/Ingredient"></c:url>?id=${ingredient.id}" class="btn btn-primary edit-button">Edytuj</a>
                <form method="POST" action="<c:url value="/Admin/Ingredient"></c:url>">
                    <input name="_method" type="hidden" value="delete">
                    <input type="hidden" name="id" value="${ingredient.id}">
                    <button class="btn btn-danger delete-button">Usuń</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>