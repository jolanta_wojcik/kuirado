<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<form method="POST" enctype="multipart/form-data">
    <input type="hidden" name="id" value="${ingredient.id}">
    <label>Nazwa</label>
    <input name="name" class="form-control" value="${ingredient.name}">
    <label>Kategoria</label>
    <select name="category" class="form-control">
        <c:forEach items="${ingredientCategories}" var="ingredientCategory">
            <option value="${ingredientCategory.name}" <c:if test="${ingredient.category == ingredientCategory.name}">selected=""</c:if>>${ingredientCategory.text}</option>
        </c:forEach>           
    </select>
    <label>Zdjęcie</label><br/>
     <c:if test="${ingredient.id != null}">
         <img src="../photos/ingredient/${ingredient.id}" width="100">
    </c:if>
    <input name="ingredientPhoto" type="file" class="form-control">
        <button id="add-ingredient-button" class="btn btn-primary">Dodaj</button>
</form>
