<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <!--- Basic Page Needs  -->
        <meta charset="utf-8">
        <title>Kuirado</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Specific Meta  -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Google Fonts -->
        <!--        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700" rel="stylesheet">-->
        <!-- CSS -->
        <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/css/font-awesome.min.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/css/owl.carousel.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/css/meanmenu.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/css/default.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/css/typography.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/css/style.css"></c:url>">
        <link rel="stylesheet" href="<c:url value="/css/responsive.css"></c:url>">
            <!-- Favicon -->
            <link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.ico"></c:url>">
        <script src="<c:url value="/js/jquery-1.12.4.min.js"></c:url>"></script>
        </head>

        <body class="banner-col3" data-spy="scroll" data-target="#scroll-menu" data-offset="100">
            <!-- header-start -->
            <header id="home">
                <div class="header-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-sm-8 col-xs-12">
                                <div class="top-left">
                                    <ul>
                                        <ul class="dropdown category">
                                        <c:forEach items="${popularTags}" var="tag">    
                                            <li><a href="<c:url value="/Tag/MostPopular"></c:url>?name=${tag.name}">${tag.name}</a></li>
                                            </c:forEach>
                                    </ul>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4 hidden-xs">
                            <div class="top-right">
                                <c:if test="${not empty user}">
                                    ${user.email}
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-middle">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-3 hidden-xs">
                            <c:choose>
                                <c:when test="${not empty user}">
                                    <div class="subscribe">
                                        <a href="<c:url value="/Logout"></c:url>" id="logout-link">Wyloguj</a>
                                        </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="subscribe" style="padding-bottom:10px">
                                        <a href="<c:url value="/Register"></c:url>" id="register-link">Zarejestruj</a>
                                        </div>
                                        <div class="subscribe">
                                            <a href="<c:url value="/Login"></c:url>" id="login-link">Login</a>
                                        </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="logo">
                                <a href="./"><img src="<c:url value="/img/logo.png"></c:url>" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-3 col-xs-12">
                                <div class="search">
                                    <input type="text">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- menu-start -->
                <div class="menu-area hidden-sm hidden-xs" id="sticker">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="menu">
                                    <nav>
                                        <ul>
                                            <li><a href="<c:url value="/"></c:url>">HOME</a>
                                            </li>
                                            <li class="dropicon"><a href="<c:url value="/Recipe"></c:url>">PRZEPISY</a>
                                                <ul class="dropdown recipe">
                                                    <li><a href="<c:url value="/Recipe"></c:url>">Katalog przepisów</a></li>
                                                <li><a href="<c:url value="/Admin/Recipe"></c:url>">Dodaj przepis</a></li>
                                                </ul>
                                            </li>
                                            <li id="ingredient-main-menu" class="dropicon"><a href="<c:url value="/Ingredient"></c:url>">SKŁADNIKI</a>
                                                <ul class="dropdown ingredient">
                                                    <li><a id="ingredients-link" href="<c:url value="/Ingredient"></c:url>">Katalog składników</a></li>
                                                <li><a id="add-ingredient-link" href="<c:url value="/Admin/Ingredient"></c:url>">Dodaj składnik</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropicon"><a href="FindRecipeByProducts">ZNAJDŻ PRZEPIS</a>
                                                <ul class="dropdown category">
                                                    <li><a href="categories-breakfast.html">Śniadanie</a></li>
                                                    <li><a href="categories-lunch.html">Obiad</a></li>
                                                    <li><a href="categories-dinner.html">Kolacja</a></li>
                                                    <li><a href="FindRecipeByProducts">Znajdź przepis po produktach</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropicon"><a href="CateringRates">KATERINGI DIETETYCZNE</a>
                                                <ul class="dropdown category">
                                                    <li><a href="CateringRates">Oceny</a></li>
                                                    <li><a href="CateringRatesAdd">Dodaj katering</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="HeatltySnaks">ZDROWE SMAKOŁYKI</a></li>
                                            <li class="dropicon"><a href="contact-us.html">KONTAKT</a>
                                                <ul class="dropdown pages">
                                                    <li><a href="about-us.html">O nas</a></li>
                                                    <li><a href="contact-us.html">Dane kontaktowe</a></li>
                                                    <li><a href="404.html">404</a></li>
                                                    <li><a href="coming-soon.html">Aktualności</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- menu-end -->
            </header>
            <!-- header-end -->
            <div class="container">
            <c:if test="${not empty errors}">
                <div class="errors" style="color: red;">
                    <c:forEach items="${errors}" var="error">
                        ${error}<br>
                    </c:forEach>
                </div>
            </c:if>
            <jsp:include page="${view}" />
        </div>
        <!-- footer-start -->
        <footer>
            <div class="footer-bottom">
                <div class="container">
                    <div class="footer-bottom-area">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="footer-copyright">
                                    <p class="copyright">@ 2016 Premium Wordpress Theme by <a target="_blank" href="#">Blogfair</a></p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="footer-social follow-box">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                    <a href="#"><i class="fa fa-youtube-play"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                    <a href="#"><i class="fa fa-rss"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="footer-right-copy">
                                    <p class="copyright">Design & Devloped by <a target="_blank" href="#">Blogfair</a></p>
                                    <a class="top-scroll smoothscroll" href="#home">TOP<span class="top-endi"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer-end -->
</html>
