<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table>
    <tr>
        <td>Name</td>
        <td>Description</td>
        <td>Preparation time</td>
        <td>Difficulty</td>
        <td>Photo</td>
        <td>Ingredients</td>
        <td>Actions</td>
    </tr>
    <c:forEach items="${recipes}" var="recipe">
        <tr>
            <td>
                <c:forEach items="${recipe.tagSet}" var="tag">#${tag.name}</c:forEach>   
            </td>
        </tr>
        <tr>
            <td class="recipe-name">${recipe.name}</td>
            <td>${recipe.description}</td>
            <td>${recipe.preparationTime}</td>
            <td>${recipe.difficultyLevelValue}</td>
            <c:if test="${flag == false}">
                <td><img src="../photos/recipe/${recipe.id}" width="100"></td>
            </c:if>
            <c:if test="${flag == true}">
                <td><img src="../photos/recipe/${recipe.id}" width="100"></td>
            </c:if>
            <td>
                Składniki:
                <ul>
                    <c:forEach items="${recipe.ingredientSet}" var="ingredient">
                        <li>${ingredient.ingredient.name} ${ingredient.amount}</li>
                    </c:forEach>
                </ul>            
            </td>
            <td>
                 <a href="<c:url value="/Admin/Recipe"></c:url>?id=${recipe.id}" class="btn btn-primary edit-button">Edytuj</a>
                <form method="POST" action="<c:url value="/Admin/Recipe"></c:url>">
                    <input name="_method" type="hidden" value="delete">
                    <input type="hidden" name="id" value="${recipe.id}">
                    <button class="btn btn-danger delete-button">Usuń</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>