<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib  prefix="cfn" uri="/WEB-INF/custom-functions.tld" %>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>

<form id="add-form" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="id" value="${recipe.id}">
    <label>Nazwa</label>
    <input name="name" class="form-control" value="${recipe.name}">
    <label>Opis</label>
    <input name="description" class="form-control" value="${recipe.description}">
    <label>Czas przygotowania</label>
    <input name="preparationTime" class="form-control" type="number" required="" value="${recipe.preparationTime}">
    <label>Trudność</label>
    <select name="difficultyLevel" class="form-control">
        <c:forEach items="${difficultyLevels}" var="difficultyLevel">
             <option value="${difficultyLevel.name}" <c:if test="${recipe.difficultyLevel == difficultyLevel.name}">selected=""</c:if>>${difficultyLevel.text}</option>
       </c:forEach>
    </select>
    <label>Dodaj tag do wyszukiwania przepisu</label><br/>
    <select name="tags" id="tags" class="js-example-basic-multiple" multiple="multiple">
        <c:forEach items="${allTags}" var="tag">
            <option value="${tag.name}" <c:if test="${  fn:contains( recipe.tagSet, tag ) }">selected="1"</c:if>>${tag.name}</option>
        </c:forEach>
    </select>
    <br/>
    <label>Zdjęcie</label><br/>
     <c:if test="${recipe.id != null}">
         <img src="../photos/recipe/${recipe.id}" width="100">
    </c:if>
    <input name="recipePhoto" type="file" class="form-control">
    <label>Składniki</label>
    <input type="hidden" name="ingredients" id="ingredients-field">
    <div id="ingredients-list" ></div>
    <span class="btn btn-primary" id="add-ingredient-button">Dodaj składnik</span>
    <button class="btn btn-primary" id="add-recipe-button" type="submit">Dodaj</button>
</form>
        
<script>
    var ingredients = '<select class="field" multiple="true" size="${fn:length(ingredients)}" required="">' +
        <c:forEach items="${ingredients}" var="ingredient">
            '<option value="${ingredient.id}" >${ingredient.name}</option>' +
        </c:forEach>
    '</select>' +
    '<input class="field"><br>';
    var ingredientsList = $("#ingredients-list");
    var button = $("#add-ingredient-button");
    var form = $("#add-form");
    var ingredientsField = $("#ingredients-field");
    button.click(function () {
        ingredientsList.append(ingredients);
    });
    form.submit(function () {
        var value = ingredientsList.find(".field").map(function (e, i) { return $(i).val(); }).toArray().join(",");
        ingredientsField.val(value);
    });
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({tags: true});
    });
</script>
