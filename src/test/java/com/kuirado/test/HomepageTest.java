package com.kuirado.test;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class HomepageTest {

    @Test
    public void homepageShows() {
        WebDriver driver = Utils.getBrowser();

        Assert.assertEquals("Kuirado", driver.getTitle());

        driver.close();
    }
}
