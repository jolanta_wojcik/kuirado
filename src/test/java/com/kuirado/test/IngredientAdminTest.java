package com.kuirado.test;

import java.util.Random;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class IngredientAdminTest {

    @Test
    public void addIngredient() {
        WebDriver driver = Utils.getBrowserWithUser();
        driver.get(Utils.getBaseUrl() + "Admin/Ingredient");

        String name = "test" + (new Random()).nextInt();
        String category = "warzywa";
        String ingredientPhotoPath = Utils.getConfig().get("TEST_FILE");

        driver.findElement(By.name("name")).sendKeys(name);
        (new Select(driver.findElement(By.name("category")))).selectByVisibleText(category);
        driver.findElement(By.name("ingredientPhoto")).sendKeys(ingredientPhotoPath);
        driver.findElement(By.id("add-ingredient-button")).click();

        Assert.assertTrue(driver.getPageSource().contains(name));

        driver.close();
    }

    //Składnik już istnieje
    @Test
    public void ingredientExists() {
        WebDriver driver = Utils.getBrowserWithUser();
        driver.get(Utils.getBaseUrl() + "Admin/Ingredient");

        String name = "jajka";

        driver.findElement(By.name("name")).sendKeys(name);
        driver.findElement(By.id("add-ingredient-button")).click();

        Assert.assertTrue(driver.getPageSource().contains("Składnik już istnieje"));

        driver.close();
    }

    @Test
    public void emptyName() {
        WebDriver driver = Utils.getBrowserWithUser();
        driver.get(Utils.getBaseUrl() + "Admin/Ingredient");

        String name = "";
        String category = "warzywa";
        String ingredientPhotoPath = Utils.getConfig().get("TEST_FILE");

        driver.findElement(By.name("name")).sendKeys(name);
        (new Select(driver.findElement(By.name("category")))).selectByVisibleText(category);
        driver.findElement(By.name("ingredientPhoto")).sendKeys(ingredientPhotoPath);
        driver.findElement(By.id("add-ingredient-button")).click();

        Assert.assertTrue(driver.getPageSource().contains("Wszytskie pola muszą być wypełnione"));

        driver.close();
    }
}
