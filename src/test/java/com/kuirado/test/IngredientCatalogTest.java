package com.kuirado.test;

import java.util.List;
import java.util.Random;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IngredientCatalogTest {

    @Test
    public void editIngredient() {
        WebDriver driver = Utils.getBrowserWithUser();
        driver.get(Utils.getBaseUrl() + "Ingredient");

        driver.findElements(By.className("edit-button")).get(0).click();

        String name = "zmiana" + (new Random()).nextInt();

        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(name);
        driver.findElement(By.id("add-ingredient-button")).click();

        Assert.assertTrue(driver.getPageSource().contains("zmiana"));

        driver.close();
    }

    @Test
    public void deleteIngredient() {
        WebDriver driver = Utils.getBrowserWithUser();

        //wejść do katalogu składników
        driver.get(Utils.getBaseUrl() + "Ingredient");

        //policzyć ile jest składników
        List<WebElement> deleteButtons = driver.findElements(By.className("delete-button"));
        int size = deleteButtons.size();

        //wybrać losowy i zapisać jego nazwę
        int toDelete = (int) (Math.random() * size);
        String name = driver.findElements(By.className("ingredient-name")).get(toDelete).getText();
        Assert.assertTrue(driver.getPageSource().contains(name));

        //usunąć wybrany
        deleteButtons.get(toDelete).click();

        //sprawdzić czy nazwa zniknęła
        Assert.assertFalse(driver.getPageSource().contains(name));

        driver.close();
    }
}
