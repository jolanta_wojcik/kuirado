package com.kuirado.test;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginTest {

    @Test
    public void correctLogin() {
        String email = "jola-tranzakcje@gmail.com";
        String password = "jola-tranzakcje@gmail.com";

        WebDriver driver = Utils.getBrowser();
        driver.findElement(By.id("login-link")).click();
        driver.findElement(By.name("email")).sendKeys(email);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.id("login-button")).click();

        Assert.assertTrue(driver.getPageSource().contains(email));

        driver.findElement(By.id("logout-link")).click();

        Assert.assertFalse(driver.getPageSource().contains(email));

        driver.close();
    }

    @Test
    public void incorrectLogin() {
        String email = "jola@gmail.com";
        String password = "zle haslo";

        WebDriver driver = Utils.getBrowser();
        driver.findElement(By.id("login-link")).click();
        driver.findElement(By.name("email")).sendKeys(email);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.id("login-button")).click();

        Assert.assertTrue(driver.getPageSource().contains("Niepoprawne hasło"));

        driver.close();
    }
}
