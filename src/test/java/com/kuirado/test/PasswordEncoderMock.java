package com.kuirado.test;

import com.kuirado.utils.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoderMock implements PasswordEncoder {

    @Override
    public Boolean matches(String password, String encryptedPassword) {
        return password.equals(encryptedPassword);
    }

    @Override
    public String encodedPassword(String encryptedPassword) {
        return (new BCryptPasswordEncoder()).encode(encryptedPassword);
    }

}
