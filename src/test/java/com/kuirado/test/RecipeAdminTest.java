package com.kuirado.test;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class RecipeAdminTest {

    @Test
    public void addRecipe() {
        WebDriver driver = Utils.getBrowserWithUser();
        driver.get(Utils.getBaseUrl() + "Admin/Recipe");

        String name = "tort";
        String description = "tort bezcukrowy";
        String preparationTime = "6";
        String photoName = Utils.getConfig().get("TEST_FILE");

        driver.findElement(By.name("name")).sendKeys(name);
        driver.findElement(By.name("description")).sendKeys(description);
        driver.findElement(By.name("preparationTime")).sendKeys(preparationTime);
        driver.findElement(By.name("recipePhoto")).sendKeys(photoName);

        driver.findElement(By.id("add-ingredient-button")).click();
        WebElement ingredients_list = driver.findElement(By.cssSelector("#ingredients-list select"));
        Select dropdown = new Select(ingredients_list);
        dropdown.selectByVisibleText("jajka");
        driver.findElement(By.id("ingredients-list")).findElement(By.tagName("input")).sendKeys("dużo");

        driver.findElement(By.id("add-recipe-button")).click();

        Assert.assertTrue(driver.getPageSource().contains("tort"));
        Assert.assertTrue(driver.getPageSource().contains("jajka dużo"));

        driver.close();
    }

}
