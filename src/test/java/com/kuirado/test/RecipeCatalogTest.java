package com.kuirado.test;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class RecipeCatalogTest {

    @Test
    public void addRecipe() {
        WebDriver driver = Utils.getBrowserWithUser();
        driver.get(Utils.getBaseUrl() + "Admin/Recipe");

        String name = "nowy" + (int) (Math.random() * 1000000);
        String description = "nowy";
        String preparationTime = "1";
//        String tag = "otaguj przepis";

        driver.findElement(By.name("name")).sendKeys(name);

        driver.findElement(By.name("description")).sendKeys(description);
        driver.findElement(By.name("preparationTime")).sendKeys(preparationTime);
        //  driver.findElement(By.name("tags")).sendKeys(tag);

        WebElement tags_list = driver.findElement(By.tagName("select"));
        Select tags_dropdown = new Select(tags_list);
        List<WebElement> listOfTagsToEdit = tags_dropdown.getOptions();
        int tagsSize = listOfTagsToEdit.size();
        int tagsToEdit = (int) (Math.random() * tagsSize);
        tags_dropdown.selectByIndex(tagsToEdit);

        //check selection of item
        driver.findElement(By.id("add-ingredient-button")).click();
        WebElement ingredients_list = driver.findElement(By.tagName("select"));
        Select dropdown = new Select(ingredients_list);

        List<WebElement> listOfElemntsToEdit = dropdown.getOptions();
        int size = listOfElemntsToEdit.size();
        int toEdit = (int) (Math.random() * size);
        String nameOfEditedElemnt = null;

        dropdown.selectByIndex(toEdit);

        (new Select(driver.findElement(By.id("ingredients-list")).findElement(By.tagName("select")))).selectByIndex(0);
        driver.findElement(By.id("ingredients-list")).findElement(By.tagName("input")).sendKeys("4");

        driver.findElement(By.id("add-recipe-button")).click();

//        JFrame  f=new JFrame();  
//        JOptionPane.showMessageDialog(f, nameOfEditedElemnt);
        Assert.assertTrue(driver.getPageSource().contains(name));

        driver.close();
    }

    @Test
    public void editRecipe() {
        WebDriver driver = Utils.getBrowserWithUser();
        driver.get(Utils.getBaseUrl() + "Recipe");

        driver.findElements(By.className("edit-button")).get(0).click();

        String name = "zmiana" + (int) (Math.random() * 1000000);;
        String description = "zmiana";
        String preparationTime = "1";
        String tag = "otaguj przepis";

        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(name);

        driver.findElement(By.name("description")).sendKeys(description);
        driver.findElement(By.name("preparationTime")).sendKeys(preparationTime);
        //  driver.findElement(By.name("tags")).sendKeys(tag);

        WebElement tags_list = driver.findElement(By.tagName("select"));
        Select tags_dropdown = new Select(tags_list);
        List<WebElement> listOfTagsToEdit = tags_dropdown.getOptions();
        int tagsSize = listOfTagsToEdit.size();
        int tagsToEdit = (int) (Math.random() * tagsSize);
        tags_dropdown.selectByIndex(tagsToEdit);

        //check selection of item
        driver.findElement(By.id("add-ingredient-button")).click();
        WebElement ingredients_list = driver.findElement(By.tagName("select"));
        Select dropdown = new Select(ingredients_list);

        List<WebElement> listOfElemntsToEdit = dropdown.getOptions();
        int size = listOfElemntsToEdit.size();
        int toEdit = (int) (Math.random() * size);
        String nameOfEditedElemnt = null;

        dropdown.selectByIndex(toEdit);

        //na około ale nie byłam w stanie inaczej wykombinować żeby mi konwertowało do Stringa
        for (WebElement element : listOfElemntsToEdit) {
            if (element.isSelected()) {
                nameOfEditedElemnt = element.getText();
            }
        }

        (new Select(driver.findElement(By.id("ingredients-list")).findElement(By.tagName("select")))).selectByIndex(0);
        driver.findElement(By.id("ingredients-list")).findElement(By.tagName("input")).sendKeys("4");

        driver.findElement(By.id("add-recipe-button")).click();

//        JFrame  f=new JFrame();  
//        JOptionPane.showMessageDialog(f, nameOfEditedElemnt);
        Assert.assertTrue(driver.getPageSource().contains(name));
//        Assert.assertTrue(driver.getPageSource().contains("otaguj przepis"));
//        Assert.assertTrue(driver.getPageSource().contains(nameOfEditedElemnt + " " + "4"));

        driver.close();
    }

    @Test
    public void deleteRecipe() {
        WebDriver driver = Utils.getBrowserWithUser();

        //wejść do katalogu składników
        driver.get(Utils.getBaseUrl() + "Recipe");

        //policzyć ile jest składników
        List<WebElement> deleteButtons = driver.findElements(By.className("delete-button"));
        int size = deleteButtons.size();

        //wybrać losowy i zapisać jego nazwę
        int toDelete = (int) (Math.random() * size);
        String name = driver.findElements(By.className("recipe-name")).get(toDelete).getText();
        Assert.assertTrue(driver.getPageSource().contains(name));

        //usunąć wybrany
        deleteButtons.get(toDelete).click();

        //sprawdzić czy nazwa zniknęła
        Assert.assertFalse(driver.getPageSource().contains(name));

        driver.close();
    }
}
