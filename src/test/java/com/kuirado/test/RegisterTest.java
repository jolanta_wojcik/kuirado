package com.kuirado.test;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterTest {

    //Użytkownik już istanieje
    @Test
    public void userExists() {
        String email = "jola-tranzakcje@gmail.com";

        WebDriver driver = Utils.getBrowser();
        driver.findElement(By.id("register-link")).click();
        driver.findElement(By.name("email")).sendKeys(email);
        driver.findElement(By.id("register-button")).click();

        Assert.assertTrue(driver.getPageSource().contains("Użytkownik już istanieje"));

        driver.close();
    }

    //Hasła są różne
    @Test
    public void passwordNotMatch() {
        String email = "jola@gmail.com";
        String password = "kotkot";
        String retype_password = "alaala";

        WebDriver driver = Utils.getBrowser();
        driver.findElement(By.id("register-link")).click();
        driver.findElement(By.name("email")).sendKeys(email);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.name("retype-password")).sendKeys(retype_password);
        driver.findElement(By.id("register-button")).click();

        Assert.assertTrue(driver.getPageSource().contains("Hasła są różne"));

        driver.close();
    }

    //Hasło jest za krótkie
    @Test
    public void passwordToShort() {
        String email = "jola@gmail.com";
        String password = "kot";

        WebDriver driver = Utils.getBrowser();
        driver.findElement(By.id("register-link")).click();
        driver.findElement(By.name("email")).sendKeys(email);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.id("register-button")).click();

        Assert.assertTrue(driver.getPageSource().contains("Hasło jest za krótkie"));

        driver.close();
    }

    //Adres email jest nie poprawny
    @Test
    public void incorrectEmail() {
        String email = "jola@";

        WebDriver driver = Utils.getBrowser();
        driver.findElement(By.id("register-link")).click();
        driver.findElement(By.name("email")).sendKeys(email);
        driver.findElement(By.id("register-button")).click();

        Assert.assertTrue(driver.getPageSource().contains("Adres email jest nie poprawny"));

        driver.close();
    }
}
