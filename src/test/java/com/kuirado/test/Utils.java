package com.kuirado.test;

import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.firefox.FirefoxDriver;

public class Utils {

    public static Dotenv getConfig() {
        String envPath = System.getProperty("user.dir");
        return Dotenv.configure().directory(envPath).load();
    }

    public static String getBaseUrl() {
        return Utils.getConfig().get("APP_URL");
    }

    public static WebDriver getBrowser() {
        System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe");
        WebDriver browser = new FirefoxDriver();
        browser.get(Utils.getBaseUrl());
        return browser;
    }

    public static WebDriver getBrowserChrome() {
        System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe");
        WebDriver browser = new ChromeDriver();
        browser.get(Utils.getBaseUrl());
        return browser;
    }

    public static WebDriver getBrowserWithUser() {
        WebDriver browser = Utils.getBrowser();
        browser.get(Utils.getBaseUrl() + "Login");
//
//        DatabaseRegisterRepository repository = new DatabaseRegisterRepository();
//
//        UserRegister userRegister = new UserRegister();
//        userRegister.setRegisterRepository(repository);
//        userRegister.register("jola@test.com", "password");

        String email = "jola-tranzakcje@gmail.com";
        String password = "jola-tranzakcje@gmail.com";

        browser.findElement(By.name("email")).sendKeys(email);
        browser.findElement(By.name("password")).sendKeys(password);
        browser.findElement(By.id("login-button")).click();

        return browser;
    }

    public static WebDriver getBrowserWithUserChrome() {
        WebDriver browser = Utils.getBrowserChrome();
        browser.get(Utils.getBaseUrl() + "Login");

        String email = "jola@gmail.com";
        String password = "haslo";

        browser.findElement(By.name("email")).sendKeys(email);
        browser.findElement(By.name("password")).sendKeys(password);
        browser.findElement(By.id("login-button")).click();

        return browser;
    }
}
