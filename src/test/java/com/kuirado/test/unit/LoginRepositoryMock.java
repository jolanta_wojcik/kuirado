package com.kuirado.test.unit;

import com.kuirado.models.User;
import java.util.LinkedList;
import java.util.List;
import com.kuirado.repository.LoginRepository;

public class LoginRepositoryMock implements LoginRepository {

    List<User> users = new LinkedList<>();

    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public User findByEmail(String email) {
        for (User u : this.users) {
            if (u.getEmail().equals(email)) {
                return u;
            }
        }
        return null;
    }

}
