package com.kuirado.test.unit;

import com.kuirado.models.User;
import com.kuirado.repository.RegisterRepository;
import com.kuirado.services.UserRegister;
import java.util.LinkedList;
import java.util.List;

public class RegisterRepositoryMock implements RegisterRepository {

    List<User> users = new LinkedList<>();

    @Override
    public User register(String email, String password) {
        for (User u : this.users) {
            u.setEmail(email);
            u.setPassword(password);
            return u;
        }
        return null;
    }

}
