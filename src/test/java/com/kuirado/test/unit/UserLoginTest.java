package com.kuirado.test.unit;

import com.kuirado.test.PasswordEncoderMock;
import com.kuirado.models.User;
import com.kuirado.services.UserLogin;
import com.kuirado.utils.PasswordEncoder;
import org.junit.Assert;
import org.junit.Test;
import com.kuirado.repository.LoginRepository;

public class UserLoginTest {

    @Test
    public void validLogin() {
        PasswordEncoder encoder = new PasswordEncoderMock();
        LoginRepository repository = new LoginRepositoryMock();

        UserLogin userLogin = new UserLogin();
        userLogin.setLoginRepository(repository);
        userLogin.setPasswordEncoder(encoder);

        LoginRepositoryMock repositoryMock = (LoginRepositoryMock) repository;
        int id = 1;
        String email = "test@gmail.com";
        String password = "secret";
        User user = new User(id, email, password);
        repositoryMock.addUser(user);

        User foundUser = userLogin.login(email, password);
        Assert.assertEquals(user, foundUser);

        foundUser = userLogin.login("nonexistigemail@gmail.com", password);
        Assert.assertNull(foundUser);

        foundUser = userLogin.login(email, password + "wrong");
        Assert.assertNull(foundUser);
    }

}
