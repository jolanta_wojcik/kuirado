package com.kuirado.test.unit;

import com.kuirado.models.User;
import com.kuirado.repository.RegisterRepository;
import com.kuirado.services.UserRegister;
import org.junit.Assert;
import org.junit.Test;

public class UserRegisterTest {

    @Test
    public void validRegister() {
        RegisterRepository repository = new RegisterRepositoryMock();

        UserRegister userRegister = new UserRegister();
        userRegister.setRegisterRepository(repository);

        RegisterRepositoryMock repositoryMock = (RegisterRepositoryMock) repository;
        String email = "test@gmail.com";
        String password = "secret";
        User user = repositoryMock.register(email, password);

        User registeredUser = userRegister.register(email, password);
        Assert.assertEquals(user, registeredUser);

        registeredUser = userRegister.register("nonexistigemail@gmail.com", password);
        Assert.assertNull(registeredUser);
    }
}
